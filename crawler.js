const request 	= require('request');
const cheerio 	= require('cheerio');
const colors 	= require('colors');
const fs 		= require('fs');


function getChordGroupsList(callback) {
	var chordGroupsList = [];
	request('http://jguitar.com/chorddictionary.jsp', function (error, response, html) {
		if (!error && response.statusCode == 200) {
			var $ = cheerio.load(html);

			$('li','.container ul').each(function(i, chordGroup) {

				$('ul li a', chordGroup).each(function(j, link) {
					chordGroupsList[chordGroupsList.length] = "http://jguitar.com"+$(link).attr("href");
				})
			});
		}
		callback(chordGroupsList);
	});
}


function parseChordVariantsInHref(href, callback) {
	var chordVariantsPages = [];
	var chordName = "";
	var chordApplications = [];
	var chordParsingIndex = 0;

	let parseChordsOnPage = function(page) {
		console.log("parse chords on page "+page);
		request(page, function (error, response, html) {
			if (!error && response.statusCode == 200) {
				var $ = cheerio.load(html);
				
				$('.col-xs-12.col-sm-6.col-md-4 img').each(function(i, image) {
					let imgSrc = $(image).attr("src");
					let imgNameParts = imgSrc.split("-");
					let lastPart = imgNameParts[imgNameParts.length-1];
					let lastPartNoPng = lastPart.split(".")[0];
					let chordStrings = lastPartNoPng.split("%2C");

					chordApplications[chordApplications.length] = chordStrings;
				});

				chordParsingIndex++;

				if (chordParsingIndex==chordVariantsPages.length) {
					callback(chordName, chordApplications);
					return;
				}
				else {
					parseChordsOnPage(chordVariantsPages[chordParsingIndex]);
				}
			}
		});
	}

	request(href, function (error, response, html) {
		if (!error && response.statusCode == 200) {
			var $ = cheerio.load(html);

			chordName = $(".h3").html().split(" ")[0];

			$('nav .pagination li a').each(function(i, page_a) {
				if (i>0 && i<($('nav .pagination li a').length-1)) {
					chordVariantsPages[chordVariantsPages.length] = "http://jguitar.com"+$(page_a).attr("href");
				}
			});

			if (chordVariantsPages.length==0) {
				chordVariantsPages[chordVariantsPages.length] = href;
			}
			console.log("parsing chord "+chordName+" from "+chordVariantsPages.length+" pages");

			parseChordsOnPage(chordVariantsPages[0]);
			
		}
	});
}

function getChordsListFromGroups(groupsLinks, callback) {
	var chordsLinks = [];
	var index = 0;

	function parseNextGroupLink() {
		console.log("Parsing group link n."+(index+1)+"/"+groupsLinks.length);
		request(groupsLinks[index], function (error, response, html) {
			if (!error && response.statusCode == 200) {
				var $ = cheerio.load(html);

				$('.list-unstyled li .col-sm-3 a').each(function(i, chord_a) {
					chordsLinks[chordsLinks.length] = "http://jguitar.com"+$(chord_a).attr("href");
				});

				index++;
				if (index<groupsLinks.length/*1*/) {
					parseNextGroupLink();
				}
				else {
					callback(chordsLinks)
				}

				console.log("Collected "+chordsLinks.length+" chord links");
			}
		});
	}
	parseNextGroupLink();
}

function writeChordToFile(chordName, chordApplications) {
	fs.writeFile("parsed/"+chordName+'.json', JSON.stringify(chordApplications, null, 4));
}
function addChordToParsedRegister(chordName) {
	fs.readFile('parsed/parsedChordsRegister.json', 'utf8', function (err, data) {
		var parsedChordsRegister = []
		if (!err) {
			parsedChordsRegister = JSON.parse(data);
		}

		if (parsedChordsRegister.indexOf(chordName)==-1) {
			parsedChordsRegister[parsedChordsRegister.length] = chordName
			fs.writeFile('parsed/parsedChordsRegister.json', JSON.stringify(parsedChordsRegister, null, 4));
		}
	});
}
function getChordsRegisterIndex(callback) {
	fs.readFile('parsed/parsedChordsRegister.json', 'utf8', function (err, data) {
		if (!err) {
			let parsedChordsRegister = JSON.parse(data);
			callback(parsedChordsRegister.length-1);
		}
		else {
			callback(0)
		}
	});
}
function parseChordsList(chordsList) {
	console.log("Begin chords list parsing");

	// var parsedChordsRegister = [];
	var index = 0;

	function parseNextChord() {
		parseChordVariantsInHref(chordsList[index], function(chordName, chordApplications) {
			writeChordToFile(chordName, chordApplications);
			addChordToParsedRegister(chordName);

			console.log("Chord parsed "+chordName+" with "+chordApplications.length+" applications");
			index++;
			if (index<chordsList.length) {
				parseNextChord()
			}
			else {
				console.log("Parsing finished");
			}
		});
	}

	getChordsRegisterIndex(function(i) {
		index = i;
		parseNextChord();
	});
}


fs.readFile('parsed/chordsLinksList.json', 'utf8', function (err, data) {
    if (err) {
    	getChordGroupsList(function(groupsLinks) {
			console.log("Got "+groupsLinks.length+" groups links");
			getChordsListFromGroups(groupsLinks, function(chordsList) {
				fs.writeFile('parsed/chordsLinksList.json', JSON.stringify(chordsList, null, 4));
				parseChordsList(chordsList);
			});
		});
    }
    else {
    	var chordsList = JSON.parse(data);
    	parseChordsList(chordsList);
    }
});
	


