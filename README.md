# README #

### jguitar.com parser ###

* Приложение занимается парсингом сайта jguitar.com и составляет базу аккордов в формате json
* Version 1.0

### Set up ###

* node.js
* npm install
* node crawler.json
* Полученные аккорды попадают в каталог parsed отдельным файлом для каждого
* Файл parsedChordsRegister.json содержит список уже полученных аккордов, чтобы не загружать их при перезапуске парсера
* Файл chordsLinksList.json содержит список ссылок на страницы аккордов, с которых идет парсинг